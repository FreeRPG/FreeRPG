
singleton Material(MossRock01_jpg)
{
   mapTo = "MossRock01_jpg";
   diffuseMap[0] = "3td_mossGranite_01_512.png";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   normalMap[0] = "3td_mossGranite_01_512_NRM.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
