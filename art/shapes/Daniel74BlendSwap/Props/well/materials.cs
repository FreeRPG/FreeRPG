
singleton Material(Well_Bricksnew)
{
   mapTo = "Bricks";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/ThePaintedWench/StoneWall1.jpg";
   specular[0] = "0.08 0.08 0.08 1";
   specularPower[0] = "100";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseColor[0] = "0.992157 0.992157 0.992157 1";
};

singleton Material(Well_Rope2)
{
   mapTo = "Rope2";
   diffuseMap[0] = "Rope2";
   normalMap[0] = "Rope2";
   specular[0] = "0.01 0.01 0.01 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(WellWater)
{
   mapTo = "WellWater";
   diffuseColor[0] = "0 0 0 1";
   specular[0] = "0.0401099 0.0547309 0.154735 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
