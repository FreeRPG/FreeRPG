
singleton Material(woodchop_AxeHead)
{
   mapTo = "AxeHead";
   diffuseMap[0] = "AxeHead";
   specular[0] = "0.4 0.4 0.4 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(woodchop_WoodenRings)
{
   mapTo = "WoodenRings";
   diffuseMap[0] = "Tree_rings";
   normalMap[0] = "Tree_rings";
   specular[0] = "0.08 0.08 0.08 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
