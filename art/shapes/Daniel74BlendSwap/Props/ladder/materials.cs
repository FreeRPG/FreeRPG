
singleton Material(ladder_Wood2)
{
   mapTo = "Wood2";
   diffuseMap[0] = "Wood";
   normalMap[0] = "Wood";
   specular[0] = "0.08 0.08 0.08 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
