
singleton Material(WindMill_nonanim__2___Default)
{
   mapTo = "_2_-_Default";
   diffuseMap[0] = "BareMetal01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};

singleton Material(WindMill_nonanim_BareMetal04)
{
   mapTo = "BareMetal04";
   diffuseMap[0] = "BareMetal04";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};

singleton Material(WindMill_Anim01_WindMillFrame02)
{
   mapTo = "WindMillFrame02";
   diffuseMap[0] = "WindMillFrame02.jpg";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   normalMap[0] = "WindMillFrame02_NRM.png";
   specularMap[0] = "WindMillFrame02_SPEC.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(WindMill_Anim01_WindMillBlades01)
{
   mapTo = "WindMillBlades01";
   diffuseMap[0] = "WindMillBlades.jpg";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "1";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   specularMap[0] = "WindMillBlades_SPEC.png";
};

singleton Material(WindMill_Anim01_WindMillFrame01)
{
   mapTo = "WindMillFrame01";
   diffuseMap[0] = "WindMillFrame01.jpg";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   normalMap[0] = "WindMillFrame01_NRM.png";
   specularMap[0] = "WindMillFrame01_SPEC.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
