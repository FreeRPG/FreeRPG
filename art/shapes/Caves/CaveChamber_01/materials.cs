
singleton Material(Cave_01_Ston024)
{
   mapTo = "Ston024";
   diffuseMap[0] = "3td_stone_015.jpg";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "96";
   translucentBlendOp = "None";
   normalMap[0] = "3td_CliffSet_01_NRM.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   parallaxScale[0] = "0";
   subSurfaceColor[0] = "0.490196 0.505882 0.509804 1";
   alphaRef = "0";
   pixelSpecular[0] = "1";
   subSurface[0] = "0";
   materialTag0 = "RoadAndPath";
};
