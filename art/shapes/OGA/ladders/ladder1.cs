
singleton TSShapeConstructor(Ladder1Dae)
{
   baseShape = "./ladder1.dae";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
