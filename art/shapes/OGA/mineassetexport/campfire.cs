
singleton TSShapeConstructor(CampfireDae)
{
   baseShape = "./campfire.dae";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
